## 安裝 VMware Workstation 15.5

在 Windows 的環境下，VMware Workstation 會是比較簡單的選擇，相較於 Oracle VirtualBox 而言，它的功能性更強大，並且可以簡單地做出 Nested Virtualization；然而，若是在 MacOS的環境下，則是使用 VMware Fusion，不過一般來說 Macbook 或是 iMac 並沒有這麼多的 RAM (記憶體)，所以用 Windows 平台可能會是比較容易的選擇。



為了能夠達成虛擬化，請務必確認你使用的 CPU 具備虛擬化功能，參考說明：[虛擬化](https://zh.wikipedia.org/wiki/虛擬化)。



![vs67_01-01](install-vmware-workstation15.assets/vs67_01-01.png)

![vs67_01-02](install-vmware-workstation15.assets/vs67_01-02.png)

![vs67_01-03](install-vmware-workstation15.assets/vs67_01-03.png)

![vs67_01-04](install-vmware-workstation15.assets/vs67_01-04.png)

![vs67_01-05](install-vmware-workstation15.assets/vs67_01-05.png)

![vs67_01-06](install-vmware-workstation15.assets/vs67_01-06.png)

![vs67_01-07](install-vmware-workstation15.assets/vs67_01-07.png)

![vs67_01-08](install-vmware-workstation15.assets/vs67_01-08.png)

![vs67_01-09](install-vmware-workstation15.assets/vs67_01-09.png)

![vs67_01-10](install-vmware-workstation15.assets/vs67_01-10.png)

![vs67_01-11](install-vmware-workstation15.assets/vs67_01-11.png)

![vs67_01-12](install-vmware-workstation15.assets/vs67_01-12.png)

![vs67_01-13](install-vmware-workstation15.assets/vs67_01-13.png)

![vs67_01-14](install-vmware-workstation15.assets/vs67_01-14.png)

![vs67_01-15](install-vmware-workstation15.assets/vs67_01-15.png)

![vs67_01-16](install-vmware-workstation15.assets/vs67_01-16.png)
